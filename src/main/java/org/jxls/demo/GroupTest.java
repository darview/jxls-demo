package org.jxls.demo;

import org.jxls.area.Area;
import org.jxls.builder.AreaBuilder;
import org.jxls.builder.xml.XmlAreaBuilder;
import org.jxls.common.CellRef;
import org.jxls.common.Context;
import org.jxls.demo.model.Department;
import org.jxls.transform.Transformer;
import org.jxls.util.TransformerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Leonid Vysochyn
 *         Date: 2/21/12 4:30 PM
 */
public class GroupTest {
    static Logger logger = LoggerFactory.getLogger(GroupTest.class);
    private static String template = "group_template.xls";
    private static String xmlConfig = "test.xml";
    private static String output = "target/group_output.xls";

    public static void main(String[] args) throws IOException {
        logger.info("Running User Command demo");
        execute();
    }

    public static void execute() throws IOException {
        List<Department> departments = EachIfCommandDemo.createDepartments();
        logger.info("Opening input stream");
        try(InputStream is = EachIfCommandDemo.class.getResourceAsStream(template)) {
            try (OutputStream os = new FileOutputStream(output)) {
                Transformer transformer = TransformerFactory.createTransformer(is, os);
                System.out.println("Creating areas");
                try (InputStream configInputStream = UserCommandDemo.class.getResourceAsStream(xmlConfig)) {
                    AreaBuilder areaBuilder = new XmlAreaBuilder(configInputStream, transformer);
                    List<Area> xlsAreaList = areaBuilder.build();
                    Area xlsArea = xlsAreaList.get(0);
                    Context context = new Context();
                    context.putVar("departments", departments);
                    logger.info("Applying area at cell " + new CellRef("Down!A1"));
                    xlsArea.applyAt(new CellRef("Down!A1"), context);
                    xlsArea.processFormulas();
                    logger.info("Complete");
                    transformer.write();
                    logger.info("Written to file");
                }
            }
        }
    }

    public static class Invoice {
        private String name;
        private InvoiceLine master;
        private List<InvoiceLine> invoice = new ArrayList<InvoiceLine>();

        public Invoice(String name, InvoiceLine master){
            this.name = name;
            this.master = master;
        }

        public static class InvoiceLine{
            private String name;
            private int count;

            public InvoiceLine(String name, int count){
                this.name = name;
                this.count = count;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public int getCount() {
                return count;
            }

            public void setCount(int count) {
                this.count = count;
            }
        }
        public void addInvoiceLine(InvoiceLine employee) {
            invoice.add(employee);
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public InvoiceLine getMaster() {
            return master;
        }

        public void setMaster(InvoiceLine master) {
            this.master = master;
        }

        public List<InvoiceLine> getInvoice() {
            return invoice;
        }

        public void setInvoice(List<InvoiceLine> invoice) {
            this.invoice = invoice;
        }
    }

    public static List<Invoice> createDepartments() {
        List<Invoice> departments = new ArrayList<>();

        Invoice.InvoiceLine chief = new Invoice.InvoiceLine("Derek", 35);
        Invoice department = new Invoice("IT", chief);
        department.addInvoiceLine(new Invoice.InvoiceLine("Elsa", 28));
        department.addInvoiceLine(new Invoice.InvoiceLine("Oleg", 32));
        departments.add(department);

        Invoice.InvoiceLine chief2 = new Invoice.InvoiceLine("Derek2", 35);
        Invoice department2 = new Invoice("IT22", chief2);
        department2.addInvoiceLine(new Invoice.InvoiceLine("Elsa2", 28));
        department2.addInvoiceLine(new Invoice.InvoiceLine("Oleg2", 32));
        departments.add(department2);

        return departments;
    }


}
